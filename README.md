# My Zettelkasten

I'm creating a Zettelkasten and I use [emanote](https://github.com/srid/neuron)
(a successor for [neuron](https://github.com/srid/neuron)) to manage it.

## How to run the webserver

To interact with the zettelkasten you can run a basic web server on the
generated zettelkasten. At the moment I am writing, emanote does not provides a
basic webserver to serve the zettelkasten.

``` sh
emanote --layers "zettelkasten" gen output
cd output && python -m http.server
```
