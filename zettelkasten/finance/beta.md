---
tags:
  - finance
---

# $\beta$ of a security

Beta is a measure of the **volatility of an asset relative to the market**. It
measures the relation of between the returns from an asset respect to the
entiere market.

Beta is just the **slope coefficient** of the fitted linear regression of the
returns of the asset respect to the market.

Therefore:

- $\beta = 1$ same volatility as the market

- $\beta > 1$ more volatile than the market

- $0 < \beta < 1$ less volatile than the market

- $\beta = 0$ uncorrelated to the market

- $\beta < 0$ negatively correlated to the market
