---
tags:
  - finance
  - statistics
---

# Covariance

Covariance is a value that measures the degree two variables change respect to
their measures. With this value we can say if it exists a dependency between the
variables.

The sign of the covariance depicts the relation between the variables:

- A **high positive** value indicates the variables have a similar behaviour.

- A **lower negative** value indicates the variables have an opposite behaviour.

- A **zero** value indicates the variables are not related.

However, the covariance is not as easy to understand. For better interpretation
we have to use a normalized version like correlation.
