---
tags:
  - finance
---

# VaR: Value at Risk

Is an indicator of risk for an asset or portfolio in a specific amount of time.

It is quantified in a liquid money, for a a probability and a time-span. For
example, a 10% one year VaR of 1000 EUR, mean there is a probability of 10% that
an asset or portfolio will decrease its value by 1000 EUR in a year.
