---
date: 2020-11-16T10:25
tags: 
  - postgres
---

# Export Postgres `bytea` column to binary file

The `\copy` allows you to export tables to your local file system. To export
`bytea` columns and export to binary file you can encode it as hexadecimal and
decode it using [[xxd]]

```bash
psql -U <USER> -h <HOST> -p <PORT> -d <DB> \
    -c "\copy (SELECT encode(<column>, 'hex') from <table> where <column> = <id>) to STDOUT" \
    | xxd -p -r > output
```
