---
date: 2021-06-30T15:05
tags: 
  - ingress
  - networking
  - kubernetes
---

# Ingress

Ingress is an API to manage external access to every [[service]] in the
cluster. It manages and proxies your request to the applications in the cluster
based on the URL of the request.

Ingress is divided by two components.

- Controller: deployed in the cluster to do reverse-proxy

- [[resources]]#: rules to configure the ingress

**Note**: The ingress controller is not deployed in the cluster by default. This
task needs to be done by the developer.
