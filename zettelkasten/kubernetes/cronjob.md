---
date: 2021-06-30T10:40
tags: 
  - kubernetes
---

# CronJob

Is a type of #[[job]] that can be schedule as in Linux CronTab. The
following snippet creates a CronJob to sum `3 + 2` schedule for every minute.

```yaml
apiVersion: batch/v1beta1
kind: CronJob
metadata:
  name: math-add-cron-job
spec:
  schedule: "*/1 * * * *"  # every minute
  jobTemplate:
    spec:
      template:
        spec:
          containers:
            - name: math-add
              image: ubuntu
              commad: ["expr", "3", "+", "2"]
              
          restartPolicy: Never
```
