---
date: 2021-04-06T15:33
tags: 
  - kubernetes
---

# Service account

Service account is an account used to authenticate a machine. For example an
application that wants to interact with the cluster. A service account is linked
to a [[secret]]. This secret contains a token used as an authentication bearer
token for the kubernetes REST api in a third-party application.

To create a service account use `kubectl`

```sh
kubectl create serviceaccount service-account-name
```

If the third-party application is already in the cluster, is possible to use the
service account specifying in the specification of the pod. The token will be
mounted as a volume and will be used automatically.

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: pod-uses-serviceaccount
spec:
  containers:
    - name: my-pod
      image: my-image

  serviceAccount: service-account-name
```

By default every pod mounts the the `default` service account. To prevent
mounting the service account set `automountServiceAccountToken: false`
