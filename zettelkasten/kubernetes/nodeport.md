---
date: 2021-06-30T14:47
tags: 
  - kubernetes
---

# NodePort

#[[service]] which listens to a port in the Kubernetes cluster and
forwards external traffic to an internal pod. External ports are between
`30000-32767` range.

The traffic is redirected randomly to the selected pods.

The following snippet creates a NodePort service to map the `30008` port to the
`80` of any pod labeled with `app: myapp`.


```yaml
apiVersion: v1
kind: Service
metadata:
  name: my-app-service
spec:
  type: NodePort
  ports:
    - targetPort: 80
      port: 80
      nodePort: 30008
  selector:
    app: myapp
```
