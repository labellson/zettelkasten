---
date: 2021-06-30T14:48
tags: 
  - kubernetes
---

# ClusterIP

#[[service]] which connect and manage **internal** traffic between 
components referring to a virtual ip instead of each single pod ip. For example
we have 3 pods for front-end and 3 for back-end. FE wants to communicate with
BE, so FE uses the cluster ip and kubernetes decides to which pod send the
request.

The following snippet creates a ClusterIP service to forward request to the port
`80` to any pod labeled with `app: myapp`.

```yaml
apiVersion: v1
kind: Service
metadata:
  name: back-end
spec:
  type: ClusterIP
  ports:
    - targetPort: 80
      port: 80
  selector:
    app: myapp
```
