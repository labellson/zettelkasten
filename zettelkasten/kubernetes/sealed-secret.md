---
date: 2021-03-02T11:19
tags: 
  - kubernetes
---

# Sealed secret

Sealed secrets are kubernetes secrets encrypted by `kubeseal`. This can be
safely stored in a public repository as only the cluster has the ability of
decrypt them.

To create a sealed secret you need to create a kubernetes secret first. Once the
secret is created you can use:

```sh
kubeseal --output yaml < cnrm-mlflow-keyfile-secret.yaml > cnrm-mlflow-keyfile-sealedsecret.yaml
```
