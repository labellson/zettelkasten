---
date: 2021-07-01T12:28
tags: 
  - storage
  - kubernetes
---

# Volumes

As pods are transient, volumes allow persistent storage in the cluster. A simple
volume can be created and mounted in a pod, specifying the path in the host,
from a config map or a [[secret]].

```yaml
spec:
  containers:
    - volumeMounts:
        - mountPath: /opt
          name: data-volume
  volumes:
  # volume from a dir in the node
  - name: data-volume
    hostPath:
      path: /data
      type: Directory
  # volume from config map
  - name: configmap-volume
    configMap:
      name: app-config
  # volume from a secret
  - name: secret-volume
    secret:
      secretName: app-secret
```

But things get complicated when the cluster is composed with more than one node
as nodes don't share the same storage. For this case, let the cluster
administrator set up a storage solution and use [[persistent-volume]] with
[[persistent-volume-claim]].
