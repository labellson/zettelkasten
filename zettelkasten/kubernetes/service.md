---
date: 2021-06-30T11:24
tags: 
  - networking
  - kubernetes
---

# Service

A service enable communication between components inside/outside of the cluster.
For example a service can be used to connect the front-end to the end users and
other to connect the back-end to an external data source.

There are different type of services available in Kubernetes:

- [[clusterip]]#

- [[nodeport]]# 

- [[loadbalancer]]#
