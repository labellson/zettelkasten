---
date: 2021-03-02T10:46
tags: 
  - kubernetes
---

# Secret

To create encrypted secrets refer to [[sealed-secret]]#

Similar to docker is possible to add a secret in kubernetes. This secret can be
queried by the engine and injected to the pods. 

One caveat is that the secrets are not safe to push into a public repository
because they are not encrypted.

You can create a secret imperative with:

```sh
kubectl create secret generic secret-name \
    --from-literal=key=value \
    --dry-run=client \
    --output yaml
```

Is also possible to add a secret from a file with `--from-file=key.json=keyfile.json`

The previous command outputs a yaml like:

```yaml
apiVersion: v1
data:
  key: dmFsdWU=
kind: Secret
metadata:
  creationTimestamp: null
  name: secret-name
  ```

Once the secret is created in the cluster, you can reference a [[secret-in-pod]]#
