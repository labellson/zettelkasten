---
date: 2021-07-01T12:36
tags: 
  - storage
  - kubernetes
---

# Persistent volumes

Persistent volumes allows to manage the storage in a central manner. This way
you can specify volume configuration in a separate file and do not have to
specify per each pod.

This way an administrator ~~or yourself~~ can create volumes for you, and you
can use them creating [[persistent-volume-claim]].

A persistent volume should specify the access mode and the storage capacity.
