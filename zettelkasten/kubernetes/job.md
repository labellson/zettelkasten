---
date: 2021-06-30T10:26
tags: 
  - kubernetes
---

# Job

Not much to explain here. A transient pod executes a task in the cluster until
the task is finished or completed. Is possible to schedule a job using
[[cronjob]]#.

The following `yaml` creates a job to add `3 + 2`.

```yaml
apiVersion: batch/v1
kind: Job
metadata:
  name: math-add-job
spec:
  template:
    spec:
      containers:
        - name: math-add
          image: ubuntu
          commad: ["expr", "3", "+", "2"]
          
      restartPolicy: Never
```

Is possible to specify how many successful completions requires the job and if
they run in parallel. For example, kubernetes will schedule 3 runs of the
following job in parallel, and will recreate the job until there are 3
successful completions if one of the previous runs fail.

```yaml
apiVersion: batch/v1
kind: Job
metadata:
  name: math-add-job
spec:
  completions: 3
  parallelism: 3
  template:
    ...
```
