---
date: 2021-04-28T16:00
tags: 
  - kubernetes
---

# Labels

Are key/value metadata assigned to a kubernetes object. Labels can be queried
using [[selectors]].

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: simple-webapp
  labels:
    app: app1
    function: front-end
```
