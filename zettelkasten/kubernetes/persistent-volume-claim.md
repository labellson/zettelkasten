---
date: 2021-07-01T12:36
tags: 
  - storage
  - kubernetes
---

# Persistent volume claim

A user can create a persistent volume claim to use a previously created
[[persistent-volume]]. A claim should include the access mode and a storage
capacity to request.

The relation between claim and volume is always one-to-one, so once there is a
binding, the volume is not available unless the claim is gone. 

When the claim is deleted. Is also possible to decide what happens to the
persistent volume. By default the policy is to `Retain` the volume but can be
also `Delete` or `Recycle` by another claim.

Kubernetes always tries to bind the best match between claims and volumes. But
is also possible to **manually select** the binding using selectors.
