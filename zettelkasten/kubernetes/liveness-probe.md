---
date: 2021-04-26T16:42
tags: 
  - kubernetes
---

# Liveness probe

Different to a [[readiness-probe]], a liveness probe indicates whether the
application is still running as expected inside of a pod.

To make sure the pod is running and the application inside the pod is working as
expected, Kubernetes will ask periodically the liveness probe of pod defined by
the developer. This can be a HTTP endpoint, a socket or a specific command.

```yaml
containers:
  - name: simple-webapp
    livenessProbe:
      initialDelaySeconds: 10
      periodSeconds: 5
      failureThreshold: 8
      httpGet:
        path: /api/ready
        port: 8080
```
