---
date: 2021-04-26T16:31
tags: 
  - kubernetes
---

# Readiness probe

Different to a [[liveness-probe]], a readiness probe indicates whether the
application running inside of a pod is ready to work or not.

To know if the application is _READY_ kubernetes query the readiness probe
defined by the developer to check the status of the application. This can be a
HTTP endpoint, a socket or a specific command.

```yaml
containers:
  - name: simple-webapp
    readinessProbe:
      httpGet:
        path: /api/ready
        port: 8080
```
