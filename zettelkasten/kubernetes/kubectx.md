---
tags: 
  - kubernetes
---

# kubectx + kubens

Switching namespace or context can be a bit painful. I don't even remember the
command anymore. Thanks there are `kubectx` and `kubens` to help you to do that.

Install it from their [repository](https://github.com/ahmetb/kubectx) and read
the short instructions to start using it.
