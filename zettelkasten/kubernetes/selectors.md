---
date: 2021-04-28T16:02
---

# Selectors

Can be used to search a group of objects that match [[labels]] inside the
kubernetes cluster. The following command will match all the pods that contains
the `app: app1`.

```sh
kubectl get pods --selector app=app1
```

Selectors are also used to connect kubernetes objects. For example to connect a
`ReplicaSet` to a group of `Pods`.

```yaml
apiVersion: apps/v1
kind: ReplicaSet
spec:
  replicas: 3
  selector:
    matchLabels:
      app: app1
      function: front-end

  template:
    metadata:
      labels:
        app: app1
        function: front-end
    spec:
      containers:
      -  name: simple-webapp
         image: simple-webapp
```
