---
date: 2021-06-30T15:25
tags: 
  - networking
  - kubernetes
  - ingress
---

# Resources

Rules to configure a #[[ingress]] controller. This rules can be configured by
host or URL path.

The following snippet creates an ingress resource to send every request to
`my-service:80`.

```yaml
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: my-ingress
spec:
  backend:
    serviceName: my-service
    servicePort: 80
```

The following snippet, forwards the traffic using the hostname or URL paths

```yaml
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: my-ingress-path
spec:
  rules:
  - host: wear.dani.codes
    http:
      paths:
      - backend:
          serviceName: wear-service
          servicePort: 80

  - http:
      paths:
      - path: /wear
      backend:
        serviceName: wear-service
        servicePort: 80
      - path: /watch
      backend:
        serviceName: watch-service
        servicePort: 80
```
