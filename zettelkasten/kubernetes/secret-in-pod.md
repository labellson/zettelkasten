---
date: 2021-04-06T13:56
tags: 
  - kubernetes
---

# Secret in a pod

Secrets can be injected in a container inside a pod as follows

## As environment variables

```yaml
envFrom:
  - secretRef:
      name: secret-name
```

## As single environment variable

```yaml
env:
  - name: password
    valueFrom:
      secretKeyRef: secret-name
      key: key-name
```

## As files in a volume

Each key in the secret will be created as a file in the directory of the
volume. Each file will contain the value of the secret.

```yaml
volumes:
  - name: secret-volume
    secret:
      secretName: secret-name
```
