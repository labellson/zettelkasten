---
date: 2021-06-30T14:48
tags: 
  - kubernetes
---

# LoadBalancer

A LoadBalancer provides a external IP adress and routes al the traffic to a
[[service]] in the cluster. Usually, you have to pay per LoadBalancer in the
different cloud platforms. That's why the LoadBalancer is used to connect all
the traffic to the cluster [[ingress]].
