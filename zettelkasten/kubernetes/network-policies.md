---
date: 2021-07-01T11:49
tags: 
  - ingress
  - networking
  - kubernetes
---

# Network policies

Set of rules to restrict and allow traffic on components in the cluster.

For kubernetes we should define two type of traffic:

- Ingress traffic: incoming traffic for a pod

- Egress traffic: outgoing traffic from a pod _(responses are not relevant)__
