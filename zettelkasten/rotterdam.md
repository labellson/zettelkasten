---
date: 2021-02-07T00:02
tags: 
  - rotterdam
  - coworking
---

# Rotterdam coworking spaces

Here there is a list of different coworking places in Rotterdam where you can
rent a flex desk and go to work.

## Startdock

Flexible desk from 8:00 am - 6:00 pm for 69€ month.

[Startdock webpage](https://startdock.nl/en/locations/rotterdam/westplein-12-overview/) 

## Spaces

Open every day for 100€ month.

[Spaces webpage](https://www.spacesworks.com/rotterdam/hofplein/#switch-language#switch-language) 

## HNK

From 8:00 - 18:00 for 15€ per day.

[HNK webpage](https://www.hnk.nl/locaties/rotterdam-centrum/) 

## The student hotel

Open every day, its also a hotel. For 99€ you can get access to the coworking,
lobby, restaurant and gym.

[The student hotel webpage](https://www.thestudenthotel.com/es/roterdam/co-working-space/) 

## The progress bar

A coworking located in the island that looks like a fork. For 119€ you can get a
month subscription.

[The progress bar webpage](https://theprogress.bar/) 

## 42workspace

Coworking from 125€ per month.

[42workspace webpage](https://www.42workspace.com/) 
