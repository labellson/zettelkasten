---
date: 2021-01-26T10:22
tags: 
  - kubernetes
  - gcloud
---

# Add access to new cluster in kubectl [^1]

In order to access a new cluster created previously in the Google [Cloud
Console](https://cloud.google.com/) you can use the following command.

```bash
gcloud container clusters get-credentials <cluster-name>
```

[^1]: [Generate Kubeconfig Entry](https://cloud.google.com/kubernetes-engine/docs/how-to/cluster-access-for-kubectl#generate_kubeconfig_entry)
