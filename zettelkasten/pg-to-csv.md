---
date: 2020-11-16T10:27
tags: 
  - postgres
---

# Postgres tables to CSV

The Postgres `\copy` command allows you to export tables to standard output or
CSV files.
