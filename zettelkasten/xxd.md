---
date: 2020-11-16T10:29
tags: 
  - hexadecimal
  - vim
---

# xxd

`xxd` is a utility from Vim to read `Hexdump`. It is possible to install it
standalone from AUR repositories in ArchLinux.

```bash
yay -S xxd-standalone
```
